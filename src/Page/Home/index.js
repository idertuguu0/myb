import React, { Component } from "react";
import css from "./style.module.css";
import Images from "../../components/Images";
import Post from "../../components/Post";
import Button from "../../components/Button";

class HomePage extends Component {
  render() {
    return (
      <div className={css.Home}>
        <Images />
        <Post />
      </div>
    );
  }
}

export default HomePage;
