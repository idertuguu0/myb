import React from "react";
import {useHistory} from 'react-router-dom'
import css from "./style.module.css";
import MenuItem from "../Menuitem";

const Menu = () => {
  const history = useHistory()

  const logout = (e) => {
    e.preventDefault()

    localStorage.removeItem('token')
    localStorage.removeItem('name')

    history.push('/')
  }

  return (
    <div>
      <ul className={css.Menu}>
        <MenuItem link="/home">Home</MenuItem>
        <MenuItem link="/singin/login">Логин</MenuItem>
        <MenuItem link="/singin">Бүртгүүлэх</MenuItem>

        <li className={css.MenuItem}>
          <a href='#' onClick={e => logout(e)}>Гарах</a>
        </li>
      </ul>
    </div>
  )
};

export default Menu;
