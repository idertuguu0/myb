import React from "react";
import css from "./style.module.css";
import Menu from "../Menu";
import TreeMenu from "../TreeMenu";

const Toolbar = (props) => {
  const name = localStorage.getItem('name')


  return (
    <header className={css.Toolbar}>
      <TreeMenu toggleSideBar={props.toggleSideBar} />

      <div>{name}</div>

      <nav className={css.HideOnMobile}>
        <Menu />
      </nav>
    </header>
  )
};

export default Toolbar;
