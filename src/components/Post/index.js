import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import Button from "../Button";
import MenuItem from "../Menuitem";
import css from "./style.module.css";
import Moment from "react-moment";
class Posts extends Component {
  state = {
    postList: [],
  };
  constructor() {
    super();
  }
  deletePost(_id) {
    axios.delete("/post/" + _id).then((response) => {
      this.componentDidMount();
    });
  }
  componentDidMount = () => {
    axios.get("/post").then((response) => {
      this.setState({
        postList: response.data,
      });
    });
  };
  render = () => {
    return (
      <section className={css.PostEdge}>
        <div className={css.Color}>
          <MenuItem link="/posts/create">Creat post</MenuItem>
        </div>

        {this.state.postList.map((post) => {
          return (
            <div className={css.Space} key={post._id}>
              <p>Name: cisco ramon </p>

              <p>
                Date: {<Moment format="YYYY/MM/DD HH:mm">{post.PostDate}</Moment>}
              </p>
              <h3>title: {post.title} </h3>
              <br />
              <div>{post.post}</div>
              <div>
                <Link className={css.Button} to={`/posts/edit/${post._id}`}>
                  Edit
                </Link>

                <Button
                  btnType="Danger"
                  text="Delete"
                  Darah={this.deletePost.bind(this, post._id)}
                />
              </div>
            </div>
          );
        })}
      </section>
    );
  };
}
export default Posts;
