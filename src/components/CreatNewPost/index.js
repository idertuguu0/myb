import React, { Component } from "react";
import { withRouter } from 'react-router-dom'
import css from "./style.module.css";
import Button from "../Button";
import axios from "axios";

// new date time ---------
let PostTime;
PostTime = new Date();
//-------------------------

class CreateNewPost extends Component {
  state = {
    title: "",
    post: "",
    PostDate: "",
  };

  createPost = (e) => {
    console.log('create post')
    const { history } = this.props

    e.preventDefault();

    let request = {
      title: document.getElementById("Title").value,
      post: document.getElementById("Post").value,
      PostDate: PostTime.toString(),
    };

    const token = localStorage.getItem('token')

    axios
      .post("/post", request, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        }
      })
      .then((resp) => {
        alert(resp.data.message);

        history.push('/home')
      })
      .catch((err) => {
        //alert("failded");
        console.log("failed");
      });
  }

  onClick() {
    window.location.href = "http://localhost:3000/home";
  }
  render() {
    return (
      <div className={css.CreatPost}>
        <form onSubmit={(e) => this.createPost(e)}>
          <h1>Create New Post</h1>
          <input
            className={css.title}
            type="text"
            placeholder="title"
            size="39"
            id="Title"
          ></input>
          <br />
          <br />
          <textarea
            className={css.Post}
            placeholder="contents"
            rows="8"
            cols="41"
            required
            id="Post"
          ></textarea>
          <br />
          <br />
          <button type="submit">Хадгалах</button>
          <button  >Буцах</button>

        </form>
      </div>
    );
  }
}


export default withRouter(CreateNewPost);
