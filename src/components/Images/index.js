import React, { useState, useCallback } from "react";
import css from "./style.module.css";
function Images() {
  const [currentImage, setCurrentImage] = useState(0);
  const [isViewerOpen, setIsViewerOpen] = useState(false);
  const images = ["http://placeimg.com/1500/700/code"];

  const openImageViewer = useCallback((index) => {
    setCurrentImage(index);
    setIsViewerOpen(true);
  }, []);

  return (
    <div className={css.Images}>
      {images.map((src, index) => (
        <img
          className={css.Images}
          src={src}
          onClick={() => openImageViewer(index)}
          key={index}
          alt="Imags"
        />
      ))}
    </div>
  );
}

export default Images;
