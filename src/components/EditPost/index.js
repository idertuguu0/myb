import React, { Component } from "react";
import Button from "../Button";
import axios from "axios";
import css from "./style.module.css";

// new date time ---------
let PostTime;
PostTime = new Date();
//-------------------------

class EditPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _id: "",
      title: "",
      post: "",
      PostDate: "",
    };
  }

  onClick() {
    window.location.href = "http://localhost:3000/home";
  }
  componentDidMount = () => {
    const postId = this.props.match.params.id;
    //fetch from db by id
    this.fetchPost(postId);
  };

  fetchPost = async (id) => {
    axios.get("/post/" + id).then((response) => {
      this.setState({
        _id: this.props.match.params.id,
        title: response.data.title,
        post: response.data.post,
      });
    });
  };
  fetchEditPost = async (iD) => {
    let request = {
      title: document.getElementById("Title").value,
      post: document.getElementById("Post").value,
      PostDate: PostTime.toString(),
    };
    axios
      .put("/post/" + iD, request)
      .then((respons) => {
        alert(respons.data.message);
      })
      .catch((err) => {
        console.log("failde");
      });
  };
  render() {
    return (
      <div className={css.EditPost}>
        <from>
          <h1>Edit Post</h1>
          <input
            onChange={(e) => {
              this.setState({ title: e.target.value });
            }}
            value={this.state.title}
            className={css.title}
            type="text"
            placeholder="title"
            size="39"
            id="Title"
          ></input>
          <br />
          <br />
          <textarea
            onChange={(e) => {
              this.setState({ post: e.target.value });
            }}
            value={this.state.post}
            className={css.Post}
            placeholder="contents"
            rows="8"
            cols="41"
            required
            id="Post"
          ></textarea>
          <br />
          <br />
          <Button
            className={css.Buttons}
            text="Save-Post"
            btnType="Success"
            Darah={this.fetchEditPost.bind(this, this.state._id)}
          />
          <Button
            className={css.Buttons}
            text="back home"
            btnType="Danger"
            Darah={this.onClick}
          />
        </from>
      </div>
    );
  }
}
export default EditPost;
