import React from "react";
import Menu from "../Menu";
import css from "./style.module.css";
import Shadow from "../Shadow";

const SideBar = (props) => {
  let classes = [css.SideBar, css.Close];

  if (props.showSidebar) {
    classes = [css.SideBar, css.Open];
  }
  return (
    <div>
      <Shadow Darah={props.toggleSideBar} show={props.showSidebar} />
      <div onClick={props.toggleSideBar} className={classes.join(" ")}>
        <Menu />
      </div>
    </div>
  );
};

export default SideBar;
