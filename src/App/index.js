import React, { Component } from "react";
//import css from "./style.module.css";
import LoginPage from "../Page/LoginPage";
import Singup from "../Page/Singup";
import Toolbar from "../components/Toolbar";
import SideBar from "../components/SideBar";
import { Switch, Route, useHistory } from "react-router-dom";
import HomePage from "../Page/Home";
import CreateNewPost from "../components/CreatNewPost";
import EditPost from "../components/EditPost";

//Cookie.set("auth", response.data.token, { path: "/home" });
class App extends Component {
  state = {
    showSidebar: false,
  };

  //const history = useHistory
  toggleSideBar = () => {
    this.setState((prevState) => {
      return { showSidebar: !prevState.showSidebar };
    });
  };
  render() {
    return (
      <div>
        <Toolbar toggleSideBar={this.toggleSideBar} />
        <SideBar
          showSidebar={this.state.showSidebar}
          toggleSideBar={this.toggleSideBar}
        />
        <main>
          <Switch>
            <Route path="/singin/login" component={LoginPage} />
            <Route path="/singin" component={Singup} />
            <Route path="/home" component={HomePage} />
            <Route path="/posts/create" component={CreateNewPost} />
            <Route path="/posts/edit/:id" component={EditPost} />
          </Switch>
          {/* <Button btnType="Success" text="Next" />
            <Button btnType="Danger" text="back" /> */}
        </main>
      </div>
    );
  }
}

export default App;

// function App() {
//   return (
//     <div>
//       <Toolbar />
//       <SideBar />
//       <main className={css.Content}></main>

//     </div>
//   );
// }

// export default App;
